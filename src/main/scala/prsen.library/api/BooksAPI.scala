package prsen.library.api

import io.swagger.annotations._
import org.slf4j.LoggerFactory
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.transaction.annotation.{Propagation, Transactional}
import org.springframework.web.bind.annotation._
import prsen.library.api.errors.{ResourceNotFoundException, ServerErrorException}
import prsen.library.model.book.Book
import prsen.library.services.BookService

@RestController
@RequestMapping(path = Array("/api/library/books-management"))
@Api(value = "Library Management API")
class BooksAPI(bookService: BookService) {

    private val log = LoggerFactory.getLogger(getClass)

    @RequestMapping(path = Array("books/{title}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get books with title")
    def getBookInfo(@RequestParam(name = "title") title: String): ResponseEntity[Book] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(bookService.getBookInfo(title).orNull)
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("books/{author}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get books with author")
    def getBooksForAuthor(@RequestParam(name = "name") name: String): ResponseEntity[java.util.List[Book]] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(bookService.getBooksForAuthor(name))
        } catch {
            case e: ServerErrorException =>  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("book/{id}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get book with id")
    def getBooksForAuthor(@RequestParam(name = "id") id: Int): ResponseEntity[Book] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(bookService.getBookForId(id).orNull)
        } catch {
            case e: ServerErrorException =>  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("book"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Add book to the database")
    @Transactional(propagation = Propagation.REQUIRED)
    def addBook(@RequestParam(name = "title") title: String,
                @RequestParam(name = "author") author: String,
                @RequestParam(name = "amount") amount: Int): ResponseEntity[Unit] = {
        try {
            bookService.addBook(title, author, amount)
            ResponseEntity.status(HttpStatus.CREATED).build()
        } catch {
            case e: ResourceNotFoundException =>  ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException =>  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("book"), method = Array(RequestMethod.DELETE))
    @ApiOperation(value = "Drop a book from database")
    @Transactional(propagation = Propagation.REQUIRED)
    def dropBook(@RequestParam(name = "id") id: Int,
                 @RequestParam(name = "amount") amount: Int): ResponseEntity[Unit] = {
        try {
            bookService.dropBook(id, amount)
            ResponseEntity.status(HttpStatus.OK).build()
        } catch {
            case e: ResourceNotFoundException =>  ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException =>  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("book"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Update book with id")
    @Transactional(propagation = Propagation.REQUIRED)
    def updateBook(@RequestParam(name = "id") id: Int,
                   @RequestParam(name = "title") title: String,
                   @RequestParam(name = "author") author: String,
                   @RequestParam(name = "amount") amount: Int): ResponseEntity[Unit] = {
        try {
            bookService.updateBookWithId(id, title, author, amount)
            ResponseEntity.status(HttpStatus.OK).build()
        } catch {
            case e: ResourceNotFoundException =>  ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException =>  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

}
