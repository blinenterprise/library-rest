package prsen.library.api

import java.sql.Date

import io.swagger.annotations.{Api, ApiOperation}
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.transaction.annotation.{Propagation, Transactional}
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, RestController}
import prsen.library.api.errors.{ForbiddenActionException, ResourceNotFoundException, ServerErrorException}
import prsen.library.model.rent.{Fee, Rent}
import prsen.library.services.RentsService

@RestController
@Api(value = "Library Management API")
@RequestMapping(path = Array("/api/library"))
class RentsAPI(rentsService: RentsService) {

    private val log: Logger = LoggerFactory.getLogger(getClass)

    @RequestMapping(path = Array("rent/{id]"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get rent with id")
    def getRentInfoForId(@RequestParam(name = "id") id: Int): ResponseEntity[Rent] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(rentsService.getRentForId(id).orNull)
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("rents/{reader-id}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get rent for reader with id")
    def getAllRentInfoForReaderId(@RequestParam(name = "id") id: Int): ResponseEntity[java.util.ArrayList[Rent]] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(rentsService.getAllRentInfoForReaderId(id))
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("rents/open/{reader-id}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get open rents for reader with id")
    def getOpenRentInfoForReaderId(@RequestParam(name = "id") id: Int): ResponseEntity[java.util.ArrayList[Rent]] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(rentsService.getOpenRentInfoForReaderId(id))
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("rent/{book-id}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get rent of book with id")
    def getRentInfoForBook(@RequestParam(name = "id") id: Int): ResponseEntity[Rent] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(rentsService.getRentInfoForBook(id))
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("rents/{date}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get rent with date")
    def getRentInfoForDate(@RequestParam(name = "date") date: Date): ResponseEntity[java.util.ArrayList[Rent]] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(rentsService.getRentInfoForDate(date))
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("rent/return"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Return a book")
    @Transactional(propagation = Propagation.REQUIRED)
    def returnBookAndCloseRent(@RequestParam(name = "id") id: Int,
                               @RequestParam(name = "is-damaged") damaged: Boolean): ResponseEntity[Fee] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(new Fee(rentsService.returnBookAndCloseRent(id, damaged)))
        } catch {
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
            case e: ForbiddenActionException => ResponseEntity.status(HttpStatus.FORBIDDEN).build()
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }
    }

    @RequestMapping(path = Array("rent/lost"), method = Array(RequestMethod.DELETE))
    @ApiOperation(value = "Notify a loss of book")
    @Transactional(propagation = Propagation.REQUIRED)
    def notifyABookLoss(@RequestParam(name = "id") id: Int): ResponseEntity[Fee] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(new Fee(rentsService.notifyABookLoss(id)))
        } catch {
            case e: Exception => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("rent/rent"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Rent a book to a reader")
    @Transactional(propagation = Propagation.REQUIRED)
    def rentBookToReader(@RequestParam(name = "reader-id") readerId: Int,
                         @RequestParam(name = "book-id") bookId: Int): ResponseEntity[Unit] = {
        try {
            rentsService.rentBookToReader(readerId, bookId)
            ResponseEntity.status(HttpStatus.OK).build()
        } catch {
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
            case e: ForbiddenActionException => ResponseEntity.status(HttpStatus.FORBIDDEN).build()
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }
    }
}
