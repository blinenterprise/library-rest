package prsen.library.api

import io.swagger.annotations.{Api, ApiOperation}
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.transaction.annotation.{Propagation, Transactional}
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, RestController}
import prsen.library.api.errors.{ForbiddenActionException, ResourceNotFoundException, ServerErrorException}
import prsen.library.model.reader.Reader
import prsen.library.services.ReadersService

@RestController
@Api(value = "Library Management API")
@RequestMapping(path = Array("/api/library/readers-management"))
class ReadersAPI(readersService: ReadersService) {

    private val log: Logger = LoggerFactory.getLogger(getClass)

    @RequestMapping(path = Array("readers/{name}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get readers with name")
    def getReaderInfoForSurname(@RequestParam(name = "reader-name") name: String): ResponseEntity[java.util.ArrayList[Reader]] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(readersService.getReaderInfoForSurname(name))
        } catch {
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("reader/{id}"), method = Array(RequestMethod.GET))
    @ApiOperation(value = "Get reader with id")
    def getReaderInfoForId(@RequestParam(name = "id") id: Int): ResponseEntity[Reader] = {
        try {
            ResponseEntity.status(HttpStatus.OK).body(readersService.getReaderForId(id).orNull)
        } catch {
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("reader"), method = Array(RequestMethod.POST))
    @ApiOperation(value = "Add reader to database")
    @Transactional(propagation = Propagation.REQUIRED)
    def addReader(@RequestParam(name = "name") name: String): ResponseEntity[Unit] = {
        try {
            readersService.addReader(name)
            ResponseEntity.status(HttpStatus.CREATED).build()
        } catch {
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    @RequestMapping(path = Array("reader"), method = Array(RequestMethod.DELETE))
    @ApiOperation(value = "Remove reader from database")
    @Transactional(propagation = Propagation.REQUIRED)
    def deleteReader(@RequestParam(name = "id") id: Int): ResponseEntity[Unit] = {
        try {
            readersService.deleteReaderForId(id)
            ResponseEntity.status(HttpStatus.OK).build()
        } catch {
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
            case e: ForbiddenActionException => ResponseEntity.status(HttpStatus.FORBIDDEN).build()
        }
    }

    @RequestMapping(path = Array("reader"), method = Array(RequestMethod.PUT))
    @ApiOperation(value = "Update reader data")
    @Transactional(propagation = Propagation.REQUIRED)
    def updateReader(@RequestParam(name = "id") id: Int,
                     @RequestParam(name = "name") name: String,
                     @RequestParam(name = "rents-number") rents: Int,
                     @RequestParam(name = "has-ever-lost-a-book") hasLost: Boolean,
                     @RequestParam(name = "has-ever-damaged-a-book") hasDamaged: Boolean): ResponseEntity[Unit] = {
        try {
            readersService.updateReaderWithId(id, name, rents, hasLost, hasDamaged)
            ResponseEntity.status(HttpStatus.OK).build()
        } catch {
            case e: ResourceNotFoundException => ResponseEntity.status(HttpStatus.NOT_FOUND).build()
            case e: ServerErrorException => ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }
}