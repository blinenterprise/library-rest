package prsen.library.api.errors

class ResourceNotFoundException(errorMessage: String = null) extends Throwable
