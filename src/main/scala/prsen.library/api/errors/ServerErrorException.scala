package prsen.library.api.errors

class ServerErrorException(errorMessage: String = null) extends Throwable
