package prsen.library.api.errors

class ForbiddenActionException(errorMessage: String = null) extends Throwable
