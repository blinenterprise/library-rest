package prsen.library.model.rent;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Fee {
    private int fee;

    public Fee(int fee) {
        this.fee = fee;
    }
}
