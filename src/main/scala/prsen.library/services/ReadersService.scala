package prsen.library.services

import org.slf4j.{Logger, LoggerFactory}
import org.springframework.stereotype.Component
import prsen.library.api.errors.{ForbiddenActionException, ResourceNotFoundException, ServerErrorException}
import prsen.library.database.repository.ReaderRepository
import prsen.library.model.reader.Reader

@Component
class ReadersService(readerRepository: ReaderRepository) {

    private val log: Logger = LoggerFactory.getLogger(getClass)

    def getReaderInfoForSurname(name: String): java.util.ArrayList[Reader] = readerRepository.findByName(name)

    def getReaderForId(id: Int): Option[Reader] = Option(readerRepository.findById(id).orElse(null))

    def addReader(name: String): Unit = {
        try {
            readerRepository.save(new Reader(name, 0))
        } catch {
            case t: Throwable =>
                log.error("Cannot create reader " + t.getMessage)
                throw new ServerErrorException("Internal server error")
        }
    }

    def deleteReaderForId(id: Int): Unit = {
        getReaderForId(id) match {
            case Some(reader) =>
                if (reader.rentsNumber > 0) {
                    log.warn("Cannot remove a reader with active rents")
                    throw new ForbiddenActionException("Cannot remove a reader with active rents")
                } else {
                    try {
                        readerRepository.delete(reader)
                    } catch {
                        case t: Throwable =>
                            log.error(s"Failed to remove reader $id")
                            throw new ServerErrorException()
                    }
                }
            case None =>
                log.error("No reader found with given id")
                throw new ResourceNotFoundException()
        }
    }

    def updateReaderWithId(id: Int, name: String, rentsNumber: Int, hasLostABook: Boolean, hasDamagedABook: Boolean) = {
        getReaderForId(id) match {
            case Some(reader) =>
                    try {
                        readerRepository.updateReader(id, name, rentsNumber, hasLostABook, hasDamagedABook)
                    } catch {
                        case t: Throwable =>
                            log.error(s"Failed to update reader $id", t)
                            throw new ServerErrorException()
                    }
            case None =>
                log.error("No reader found with given id")
                throw new ResourceNotFoundException()
        }
    }
}
