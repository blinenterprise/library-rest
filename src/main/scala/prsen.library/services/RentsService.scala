package prsen.library.services

import java.sql.Date
import java.util.concurrent.TimeUnit

import org.slf4j.{Logger, LoggerFactory}
import org.springframework.stereotype.Component
import prsen.library.api.errors.{ForbiddenActionException, ResourceNotFoundException, ServerErrorException}
import prsen.library.database.repository.{BookRepository, ReaderRepository, RentRepository}
import prsen.library.model.rent.Rent

@Component
class RentsService(rentRepository: RentRepository,
                   readerRepository: ReaderRepository,
                   bookRepository: BookRepository,
                   bookService: BookService,
                   readersService: ReadersService) {

    private val log: Logger = LoggerFactory.getLogger(getClass)

    def getRentForId(id: Int): Option[Rent] = Option(rentRepository.findById(id).orElse(null))

    def getAllRentInfoForReaderId(id: Int): java.util.ArrayList[Rent] = rentRepository.findByReaderId(id)

    def getOpenRentInfoForReaderId(id: Int): java.util.ArrayList[Rent] = rentRepository.findByReaderIdAndIsClosed(id, false)

    def getRentInfoForBook(id: Int): Rent = rentRepository.findByBookId(id)

    def getRentInfoForDate(date: Date): java.util.ArrayList[Rent] = rentRepository.findByRentalDate(date)

    def returnBookAndCloseRent(id: Int, damaged: Boolean): Int = {

        val maybeBook = bookService.getBookForId(id)
        val maybeRent = getRentForId(id)
        (maybeBook, maybeRent) match {
            case (Some(book), Some(rent)) =>
                if (!rent.isClosed) {
                    try {
                        val reader = readerRepository.findById(rent.readerId).orElse(null)
                        val now = new Date(new java.util.Date().getTime)
                        readerRepository.decreaseRentNumberById(reader.id)
                        if (damaged)
                            readerRepository.setDamagedById(reader.id, true)
                        bookRepository.increaseAmountInStockByOneForId(book.id)
                        val fee: Int = calculateFee(rent.rentalDate, now)
                        rentRepository.setClosedFor(true, rent.id)
                        rentRepository.setClosingDateFor(rent.id, new java.util.Date())
                        rentRepository.setFeeFor(rent.id, fee)
                        fee
                    } catch {
                        case t: Throwable =>
                            log.error(s"Failed to close rent for book $id" + t.getMessage)
                            throw new ServerErrorException()
                    }
                } else {
                    log.warn("This rent is closed")
                    throw new ForbiddenActionException()
                }
            case (_, _) =>
                log.info(s"Either rent or book does not exist")
                throw new ResourceNotFoundException()
        }
    }

    def notifyABookLoss(bookId: Int): Int = {
        try {
            val rent = rentRepository.findByBookId(bookId)
            val reader = readerRepository.findById(rent.readerId).orElse(null)
            val now = new Date(new java.util.Date().getTime)
            readerRepository.decreaseRentNumberById(reader.id)
            readerRepository.updateReader(reader.id, reader.name, reader.rentsNumber, true, reader.hasEverDamagedABook)
            rentRepository.setClosedFor(true, rent.id)
            rentRepository.setClosingDateFor(rent.id, now)
            // Fee for lost book is 40 PLN
            val fee: Int = calculateFee(rent.rentalDate, now) + 40
            rentRepository.setFeeFor(rent.id, fee)
            fee
        } catch {
            case t: Throwable =>
                log.error(s"Failed to notify a book loss for book $bookId")
                throw new ServerErrorException()
        }
    }

    def rentBookToReader(readerId: Int, bookId: Int): Unit = {
        val maybeReader = readersService.getReaderForId(readerId)
        val maybeBook = bookService.getBookForId(bookId)

        (maybeBook, maybeReader) match {
            case (Some(book), Some(reader)) =>
                if (book.amountInStock > 0) {
                    try {
                        rentRepository.save(new Rent(reader.id, book.id, new Date(new java.util.Date().getTime), false))
                        bookRepository.decreaseAmountInStockByOneForId(book.id)
                        readerRepository.increaseRentNumberById(reader.id)
                    } catch {
                        case t: Throwable =>
                            log.error(s"Failed to rent book $bookId to reader $readerId", t)
                            throw new ServerErrorException()
                    }
                } else {
                    log.info(s"There is no book $bookId in stock")
                    throw new ForbiddenActionException()
                }

            case (_, _) =>
                log.error(s"Either reader $readerId or book $bookId does not exist")
                throw new ResourceNotFoundException()
        }
    }

    private def calculateFee(startingDate: Date, closingDate: Date): Int = {
        // Assuming, that allowed rental time is 30 days and fee is equal to 1 PLN for each day above 30
        val utilStartDate = new java.util.Date(startingDate.getYear, startingDate.getMonth, startingDate.getDay)
        val utilEndDate = new java.util.Date(closingDate.getYear, closingDate.getMonth, closingDate.getDay)
        val diff = utilEndDate.getTime - utilStartDate.getTime
        val diffDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).asInstanceOf[Int]
        if (diffDays < 30) 0 else diffDays - 30

    }
}
