package prsen.library.services

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import prsen.library.api.errors.{ResourceNotFoundException, ServerErrorException}
import prsen.library.database.repository.BookRepository
import prsen.library.model.book.Book

@Component
class BookService(bookRepository: BookRepository) {

    private val log = LoggerFactory.getLogger(getClass)

    def getBookInfo(title: String): Option[Book] = bookRepository.findByTitle(title)

    def getBooksForAuthor(name: String): java.util.ArrayList[Book] = bookRepository.findByAuthor(name)

    // In addiction to searching a book, this function can be used to convert java optional to scala option
    def getBookForId(id: Int): Option[Book] = Option(bookRepository.findById(id).orElse(null))

    def addBook(title: String, author: String, amount: Int): Unit = {
        try {
            bookRepository.findByTitle(title) match {
                case Some(book) => bookRepository.increaseAmountForId(book.id, amount)
                case None => bookRepository.save(new Book(title, author, amount))
            }
        } catch {
            case t: Throwable =>
                log.error(s"Failed to add a book with title $title", t)
                throw new ServerErrorException("Internal server error")
        }

    }

    def dropBook(id: Int, amount: Int): Unit = {
        getBookForId(id) match {
            case Some(book) =>
                try {
                    if (book.amountInStock - amount <= 0)
                        bookRepository.delete(book)
                    else
                        bookRepository.dropAmountForId(book.id, amount)
                } catch {
                    case t: Throwable =>
                        log.error(s"Failed to remove book with id $id", t)
                        throw new ServerErrorException("Internal server error")
                }
            case None =>
                log.info(s"Book with id $id not found in database")
                throw new ResourceNotFoundException()
        }
    }

    def updateBookWithId(id: Int, title: String, author: String, amount: Int): Unit = {
        getBookForId(id) match {
            case Some(book) =>
                try {
                    bookRepository.updateBook(id, title, author, amount)
                } catch {
                    case t: Throwable =>
                        log.error(s"Failed to update book with id $id", t)
                        throw new ServerErrorException("Internal server error")
                }
            case None =>
                log.info(s"Book with id $id not found in database")
                throw new ResourceNotFoundException()
        }
    }
}
