package prsen.library.database.repository

import org.springframework.data.jpa.repository.{Modifying, Query}
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component
import prsen.library.model.book.Book

import scala.tools.nsc.doc.base.comment.Title

@Component
trait BookRepository extends CrudRepository[Book, Int] {

    def findByTitle(title: String): Option[Book]

    def findByAuthor(author: String): java.util.ArrayList[Book]

    @Modifying
    @Query("UPDATE Book book SET book.amountInStock = (SELECT amountInStock FROM Book WHERE id = ?1)-1 WHERE book.id = ?1")
    def decreaseAmountInStockByOneForId(id: Int): Int

    @Modifying
    @Query("UPDATE Book book SET book.amountInStock = (SELECT amountInStock FROM Book WHERE id = ?1)+1 WHERE book.id = ?1")
    def increaseAmountInStockByOneForId(id: Int): Int

    @Modifying
    @Query("UPDATE Book book SET book.amountInStock = (SELECT amountInStock FROM Book WHERE id = ?1)-?2 WHERE book.id = ?1")
    def dropAmountForId(id: Int, amount: Int): Int

    @Modifying
    @Query("UPDATE Book book SET book.amountInStock = (SELECT amountInStock FROM Book WHERE id = ?1)+?2 WHERE book.id = ?1")
    def increaseAmountForId(id: Int, amount: Int): Int

    @Modifying
    @Query("UPDATE Book book SET book.title = ?2, book.author = ?3, book.amountInStock = ?4 WHERE book.id = ?1")
    def updateBook(id: Int, title: String, author: String, amount: Int): Unit

}
