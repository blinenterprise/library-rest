package prsen.library.database.repository

import org.springframework.data.jpa.repository.{JpaRepository, Modifying, Query}
import org.springframework.stereotype.Component
import prsen.library.model.reader.Reader

@Component
trait ReaderRepository extends JpaRepository[Reader, Int]{
    
    def findByName(name: String) : java.util.ArrayList[Reader]
    
    @Modifying
    @Query("UPDATE Reader reader SET reader.rentsNumber = (SELECT rentsNumber FROM Reader WHERE id = ?1)-1 WHERE reader.id = ?1")
    def decreaseRentNumberById(id: Int) : Int
    
    @Modifying
    @Query("UPDATE Reader reader SET reader.rentsNumber = (SELECT rentsNumber FROM Reader WHERE id = ?1)+1 WHERE reader.id = ?1")
    def increaseRentNumberById(id: Int) : Int
    
    @Modifying
    @Query("UPDATE Reader reader SET reader.hasEverDamagedABook = ?2 WHERE reader.id = ?1")
    def setDamagedById(id: Int, damaged: Boolean) : Int
    
    @Modifying
    @Query("UPDATE Reader reader SET reader.hasEverLostABook = ?2 WHERE reader.id = ?1")
    def setLostById(id: Int, lost: Boolean) : Int

    @Modifying
    @Query("UPDATE Reader reader SET reader.name = ?2, reader.rentsNumber = ?3, reader.hasEverDamagedABook = ?5, reader.hasEverLostABook = ?4 WHERE reader.id = ?1")
    def updateReader(id: Int, name: String, rentsNumber: Int, lostBook: Boolean, damagedBook: Boolean) : Int
}
