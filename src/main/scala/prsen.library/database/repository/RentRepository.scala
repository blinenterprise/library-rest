package prsen.library.database.repository

import java.sql.Date

import org.springframework.data.jpa.repository.{JpaRepository, Modifying, Query}
import org.springframework.stereotype.Component
import prsen.library.model.rent.Rent

@Component
trait RentRepository extends JpaRepository[Rent, Int]{
    
    def findByRentalDate(date: Date) : java.util.ArrayList[Rent]
    
    def findByBookId(id: Int) : Rent
    
    def findByReaderId(id: Int) : java.util.ArrayList[Rent]
    
    def findByIsClosed(isClosed: Boolean) : java.util.ArrayList[Rent]
    
    def findByReaderIdAndIsClosed(id: Int, isClosed: Boolean) : java.util.ArrayList[Rent]
    
    @Modifying
    @Query("UPDATE Rent rent SET rent.isClosed = ?1 WHERE rent.id = ?2")
    def setClosedFor(isClosed : Boolean, rentId: Int): Int
    
    @Modifying
    @Query("UPDATE Rent rent SET rent.closeDate = ?2 WHERE rent.id = ?1")
    def setClosingDateFor(rentId: Int, date: java.util.Date): Int
    
    @Modifying
    @Query("UPDATE Rent rent SET rent.feeInPLN = ?2 WHERE rent.id = ?1")
    def setFeeFor(rentId: Int, fee: Int): Int
}
