package prsen.library

import java.text.SimpleDateFormat

import org.slf4j.{Logger, LoggerFactory}
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import prsen.library.database.repository.{BookRepository, ReaderRepository, RentRepository}
import prsen.library.model.book.Book
import prsen.library.model.reader.Reader
import prsen.library.model.rent.Rent

@SpringBootApplication
class WebServer

object WebServer extends App {
    
    SpringApplication.run(classOf[WebServer], args: _*)

    private val log : Logger = LoggerFactory.getLogger(getClass)

    // TODO: move initializing methods out of web server
    @Bean
    def initializeRents(rentsRepo: RentRepository,
                        booksRepo: BookRepository,
                        readersRepo: ReaderRepository): Boolean = {
        try{
            val reader = readersRepo.findByName("Roman Stanisławski").get(0)
            val book1 = booksRepo.findByTitle("Achaja")
            val book2 = booksRepo.findByTitle("Pomnik Cesarzowej Achai")
            val format = new SimpleDateFormat("YYYY-MM-DD")
            rentsRepo.save(new Rent(reader.id, book1.get.id, new java.sql.Date(format.parse("2018-03-15").getTime), false))
            rentsRepo.save(new Rent(reader.id, book2.get.id, new java.sql.Date(format.parse("2018-01-30").getTime), false))
            true
        } catch {
            case t: Throwable =>
                log.error("Failed to initialize readers repository")
                false
        }
    }

    @Bean
    def initializeBooks(repo: BookRepository): Boolean = {
        try {
            repo.save(new Book("Virion", "Andrzej Ziemiański", 1))
            repo.save(new Book("Gra o Tron", "George R. R. Martin", 1))
            repo.save(new Book("Achaja", "Andrzej Ziemiański", 2))
            repo.save(new Book("Pomnik Cesarzowej Achai", "Andrzej Ziemiański", 1))
            repo.save(new Book("Siewca Wiatru", "Maja Lidia Kossakowska", 1))
            repo.save(new Book("Kroniki Jakuba Wędrowycza", "Andrzej Pilipiuk", 4))
            true
        } catch {
            case t: Throwable =>
                log.error("Failed to initialize books repository")
                false
        }
    }

    @Bean
    def initializeReaders(repo: ReaderRepository): Boolean = {
        try{
            repo.save(new Reader("Jan Kowalski",  0))
            repo.save(new Reader("Stanisław Janowski", 0))
            repo.save(new Reader("Roman Stanisławski", 2))
            true
        } catch {
            case t: Throwable =>
                log.error("Failed to initialize readers repository")
                false
        }
    }
    
    override def main(args: Array[String]): Unit = {
        
        super.main(args)
        
        // TODO: write configuration container and get local ip from maven repository properties
        log.info("Serving at 127.0.0.1:8080/swagger-ui.html")
    }
}
